#use wml::debian::template title="Security Information" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Keeping your Debian System secure</a></li>
<li><a href="#DSAS">Recent Advisories</a></li>
<li><a href="#infos">Sources of Security Information</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian takes security very seriously. We handle all security problems brought to our attention and ensure that they are corrected within a reasonable timeframe.</p>
</aside>

<p>
Experience has shown that <q>security through obscurity</q> never works. Therefore, public disclosure allows for quicker and better solutions of security problems. In that respect, this page addresses Debian's status regarding various known security holes, which could potentially affect the Debian operating system.
</p>

<p>
The Debian project coordinates many security advisories with other free software vendors, and as a result, these advisories are published the same day a vulnerability is made public.
In order to receive the latest Debian security advisories, please subscribe to the <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a> mailing list.
</p>


</p>

# "reasonable timeframe" might be too vague, but we don't have
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.


<p>
Debian also participates in security standardization efforts:
</p>

<ul>
  <li>The <a href="#DSAS">Debian Security Advisories</a> are <a href="cve-compatibility">CVE-Compatible</a> (review the <a href="crossreferences">cross references</a>).</li>
  <li>Debian <a href="oval/">publishes</a> its security information using the <a href="https://github.com/CISecurity/OVALRepo">Open Vulnerability Assessment Language (OVAL)</a></li>
</ul>

<h2><a id="keeping-secure">Keeping your Debian System secure</a></h2>



<p>

The packages <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a> is installed with the GNOME desktop
and keeps the computer current with the latest security (and other) updates automatically.

The <a href="https://wiki.debian.org/UnattendedUpgrades">wiki entry</a> has
more detailed information how to manually set up <tt>unattended-upgrades</tt>.

<p>
For more information about security issues in Debian, please refer to our FAQ and our documentation:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">Security FAQ</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Securing Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Recent Advisories</a></h2>

<p>These are the recent Debian Security Advisories (DSA) posted to the <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a> list.
<br><b>T</b> is the link to the <a href="https://security-tracker.debian.org/tracker">Debian Security Tracker</a> information, the DSA number links to the announcement mail.

<p>
#include "$(ENGLISHDIR)/security/dsa.list"
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<h2><a id="infos">Sources of Security Information</a></h2>
#include "security-sources.inc"

<p>
The latest Debian security advisories are available as <a href="dsa">RDF files</a>. We also offer a slightly <a href="dsa-long">longer version</a> of the files which includes the first paragraph of the corresponding advisory.
</p>
