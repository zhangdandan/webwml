<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2101">CVE-2019-2101</a>

    <p>Andrey Konovalov discovered that the USB Video Class driver
    (uvcvideo) did not consistently handle a type field in device
    descriptors, which could result in a heap buffer overflow.  This
    could be used for denial of service or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10639">CVE-2019-10639</a>

    <p>Amit Klein and Benny Pinkas discovered that the generation of IP
    packet IDs used a weak hash function that incorporated a kernel
    virtual address.  In Linux 3.16 this hash function is not used for
    IP IDs but is used for other purposes in the network stack.  In
    custom kernel configurations that enable kASLR, this might weaken
    kASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13272">CVE-2019-13272</a>

    <p>Jann Horn discovered that the ptrace subsystem in the Linux kernel
    mishandles the management of the credentials of a process that wants
    to create a ptrace relationship, allowing a local user to obtain root
    privileges under certain scenarios.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in
version 3.16.70-1.  This update also fixes a regression introduced by
the original fix for <a
href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a>
(<a href="https://bugs.debian.org/930904">#930904</a>), and includes
other fixes from upstream stable updates.</p>

<p>We recommend that you upgrade your linux and linux-latest
packages.  You will need to use "apt-get upgrade --with-new-pkgs"
or <q>apt upgrade</q> as the binary package names have changed.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1862.data"
# $Id: $
