<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Chris Coulson discovered a flaw in systemd leading to denial of service.
An unprivileged user could take advantage of this issue to crash PID1 by
sending a specially crafted D-Bus message on the system bus.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
215-17+deb8u10.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1684.data"
# $Id: $
