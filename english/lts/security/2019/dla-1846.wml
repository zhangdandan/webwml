<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>David Fifield discovered a way to construct non-recursive <q>zip bombs</q>
that achieve a high compression ratio by overlapping files inside the
zip container. However the output size increases quadratically in the
input size, reaching a compression ratio of over 28 million
(10 MB -&gt; 281 TB) at the limits of the zip format which can cause a
denial-of-service. Mark Adler provided a patch to detect and reject
such zip files for the unzip program.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
6.0-16+deb8u4.</p>

<p>We recommend that you upgrade your unzip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1846.data"
# $Id: $
