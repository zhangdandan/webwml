<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been resolved in libjpeg-turbo, Debian's
default JPEG implemenation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3616">CVE-2016-3616</a>

    <p>The cjpeg utility in libjpeg allowed remote attackers to cause a
    denial of service (NULL pointer dereference and application crash) or
    execute arbitrary code via a crafted file.</p>

    <p>This issue got fixed by the same patch that fixed <a href="https://security-tracker.debian.org/tracker/CVE-2018-11213">CVE-2018-11213</a> and
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-11214">CVE-2018-11214</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1152">CVE-2018-1152</a>

    <p>libjpeg-turbo has been found vulnerable to a denial of service
    vulnerability caused by a divide by zero when processing a crafted
    BMP image. The issue has been resolved by a boundary check.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11212">CVE-2018-11212</a>

    <p>The alloc_sarray function in jmemmgr.c allowed remote attackers to
    cause a denial of service (divide-by-zero error) via a crafted file.</p>

    <p>The issue has been addressed by checking the image size when reading
    a targa file and throwing an error when image width or height is 0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11213">CVE-2018-11213</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-11214">CVE-2018-11214</a>

    <p>The get_text_gray_row and get_text_rgb_row functions in rdppm.c both
    allowed remote attackers to cause a denial of service (Segmentation
    fault) via a crafted file.</p>

    <p>By checking the range of integer values in PPM text files and adding
    checks to ensure values are within the specified range, both issues</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:1.3.1-12+deb8u1.</p>

<p>We recommend that you upgrade your libjpeg-turbo packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1638.data"
# $Id: $
