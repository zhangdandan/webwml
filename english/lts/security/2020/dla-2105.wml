<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tom Lane discovered that <q>ALTER ... DEPENDS ON EXTENSION</q> sub commands
in the PostgreSQL database did not perform authorisation checks.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
9.4.26-0+deb8u1.</p>

<p>We recommend that you upgrade your postgresql-9.4_9.4.26-0+deb8u1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2105.data"
# $Id: $
