<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in gpac, a multimedia framework featuring
the MP4Box muxer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21015">CVE-2018-21015</a>

    <p>AVC_DuplicateConfig() at isomedia/avc_ext.c allows remote
    attackers to cause a denial of service (NULL pointer dereference
    and application crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21016">CVE-2018-21016</a>

    <p>audio_sample_entry_AddBox() at isomedia/box_code_base.c allows
    remote attackers to cause a denial of service (heap-based buffer
    over-read and application crash) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13618">CVE-2019-13618</a>

    <p>isomedia/isom_read.c in libgpac.a has a heap-based buffer
    over-read, as demonstrated by a crash in gf_m2ts_sync in
    media_tools/mpegts.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20161">CVE-2019-20161</a>

    <p>heap-based buffer overflow in the function
    ReadGF_IPMPX_WatermarkingInit() in odf/ipmpx_code.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20162">CVE-2019-20162</a>

    <p>heap-based buffer overflow in the function gf_isom_box_parse_ex()
    in isomedia/box_funcs.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20163">CVE-2019-20163</a>

    <p>NULL pointer dereference in the function gf_odf_avc_cfg_write_bs()
    in odf/descriptors.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20165">CVE-2019-20165</a>

    <p>NULL pointer dereference in the function ilst_item_Read() in
    isomedia/box_code_apple.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20170">CVE-2019-20170</a>

    <p>invalid pointer dereference in the function GF_IPMPX_AUTH_Delete()
    in odf/ipmpx_code.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20171">CVE-2019-20171</a>

    <p>memory leaks in metx_New in isomedia/box_code_base.c and abst_Read
    in isomedia/box_code_adobe.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20208">CVE-2019-20208</a>

    <p>dimC_Read in isomedia/box_code_3gpp.c in GPAC 0.8.0 has a
    stack-based buffer overflow.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.5.0+svn5324~dfsg1-1+deb8u5.</p>

<p>We recommend that you upgrade your gpac packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2072.data"
# $Id: $
