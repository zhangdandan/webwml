<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues were discovered in dovecot: IMAP and POP3 email server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33515">CVE-2021-33515</a>

    <p>The submission service in Dovecot before 2.3.15 allows STARTTLS command
    injection in lib-smtp. Sensitive information can be redirected to an
    attacker-controlled address.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30550">CVE-2022-30550</a>

    <p>When two passdb configuration entries exist with the same driver and args
    settings, incorrectly applied settings can lead to an unintended security
    configuration and can permit privilege escalation in certain configurations.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1:2.3.4.1-5+deb10u7.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>For the detailed security status of dovecot please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dovecot">https://security-tracker.debian.org/tracker/dovecot</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3122.data"
# $Id: $
