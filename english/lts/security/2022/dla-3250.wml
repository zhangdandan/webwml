<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in multipath-tools, a tool-chain to manage disk
multipath device maps, which may be used by local attackers to obtain root
privileges or create a directories or overwrite files via symlink attacks.</p>

<p>Please note that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2022-41973">CVE-2022-41973</a> involves switching from
/dev/shm to systemd-tmpfiles (/run/multipath-tools).
If you have previously accesssed /dev/shm directly, please update your
setup to the new path to facilitate this change.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41973">CVE-2022-41973</a>

    <p>multipath-tools 0.7.7 through 0.9.x before 0.9.2 allows local users to
    obtain root access, as exploited in conjunction with <a href="https://security-tracker.debian.org/tracker/CVE-2022-41974">CVE-2022-41974</a>.
    Local users able to access /dev/shm can change symlinks in multipathd
    due to incorrect symlink handling, which could lead to controlled file
    writes outside of the /dev/shm directory. This could be used indirectly
    for local privilege escalation to root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41974">CVE-2022-41974</a>

    <p>multipath-tools 0.7.0 through 0.9.x before 0.9.2 allows local users to
    obtain root access, as exploited alone or in conjunction with
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-41973">CVE-2022-41973</a>. Local users able to write to UNIX domain sockets can
    bypass access controls and manipulate the multipath setup. This can lead
    to local privilege escalation to root. This occurs because an attacker
    can repeat a keyword, which is mishandled because arithmetic ADD is used
    instead of bitwise OR.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.7.9-3+deb10u2.</p>

<p>We recommend that you upgrade your multipath-tools packages.</p>

<p>For the detailed security status of multipath-tools please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/multipath-tools">https://security-tracker.debian.org/tracker/multipath-tools</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3250.data"
# $Id: $
