<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>ooooooo_q discovered that the actionpack_page-caching Ruby gem, a
static page caching module for Rails, allows an attacker to write
arbitrary files to a web server, potentially resulting in remote code
execution if the attacker can write unescaped ERB to a view.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.0.2-4+deb9u1.</p>

<p>We recommend that you upgrade your ruby-actionpack-page-caching packages.</p>

<p>For the detailed security status of ruby-actionpack-page-caching please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-actionpack-page-caching">https://security-tracker.debian.org/tracker/ruby-actionpack-page-caching</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2719.data"
# $Id: $
