<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were found in MediaWiki, a website engine for
collaborative work, which could result in cross-site scripting,
denial of service and certain unintended API access.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1:1.27.7-1~deb9u10.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2779.data"
# $Id: $
