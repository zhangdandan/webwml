<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>During the backporting of one of patches in <a href="https://security-tracker.debian.org/tracker/CVE-2020-22021">CVE-2020-22021</a> one line was wrongly
interpreted and it caused the regression during the deinterlacing process.
Thanks to Jari Ruusu for the reporting the issue and for the testing of
prepared update.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
7:3.2.15-0+deb9u4.</p>

<p>We recommend that you upgrade your ffmpeg packages.</p>

<p>For the detailed security status of ffmpeg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ffmpeg">https://security-tracker.debian.org/tracker/ffmpeg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2742-2.data"
# $Id: $
