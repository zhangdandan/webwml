<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A heap overflow issue was detected in libebml, a library to read and
write files in the EBML format, a binary pendant to XML.
These issues appeared in several ReadData functions of various data type
classes. This update also fixes the issue in EbmlString::ReadData and
EbmlUnicodeString::ReadData, which were mentioned in <a href="https://security-tracker.debian.org/tracker/CVE-2021-3405">CVE-2021-3405</a>.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.3.4-1+deb9u2.</p>

<p>We recommend that you upgrade your libebml packages.</p>

<p>For the detailed security status of libebml please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libebml">https://security-tracker.debian.org/tracker/libebml</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2629.data"
# $Id: $
