<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libbsd, a library with utility functions from
BSD systems.
A non-NUL terminated symbol name in the string table might result in an
out-of-bounds read.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
0.8.3-1+deb9u1.</p>

<p>We recommend that you upgrade your libbsd packages.</p>

<p>For the detailed security status of libbsd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libbsd">https://security-tracker.debian.org/tracker/libbsd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2566.data"
# $Id: $
