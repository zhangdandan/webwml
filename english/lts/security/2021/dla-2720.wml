<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issue have been found in aspell, the GNU Aspell spell-checker.</p>

<p>One issue is related to a stack-based buffer over-read via an isolated \
character when processing a configuration file.
The other issue is related to a heap-based buffer overflow.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
0.60.7~20110707-3+deb9u1.</p>

<p>We recommend that you upgrade your aspell packages.</p>

<p>For the detailed security status of aspell please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/aspell">https://security-tracker.debian.org/tracker/aspell</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2720.data"
# $Id: $
