<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in openssl, a Secure Sockets Layer toolkit.
Ingo Schwarze reported a buffer overrun flaw when processing ASN.1
strings, which can result in denial of service.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.1.0l-1~deb9u4.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">https://security-tracker.debian.org/tracker/openssl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2766.data"
# $Id: $
