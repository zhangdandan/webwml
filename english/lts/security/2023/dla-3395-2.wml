<define-tag description>LTS regression update</define-tag>
<define-tag moreinfo>
<p>The previous golang-1.11 update released as DLA-3395-1 failed to build
reliably on the armhf architecture, due to intermittent ("flaky") test
cases in the test suite, which is run on package build as part of
Debian quality assurance. This update disables a few such tests and
has otherwise no impact on the published golang-1.11 package.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.11.6-1+deb10u7.</p>

<p>We recommend that you upgrade your golang-1.11 packages.</p>

<p>For the detailed security status of golang-1.11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-1.11">https://security-tracker.debian.org/tracker/golang-1.11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3395-2.data"
# $Id: $
