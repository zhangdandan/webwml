<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Python 3.7.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48560">CVE-2022-48560</a>

    <p>A use-after-free problem was found in the heappushpop function in
    the heapq module.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48564">CVE-2022-48564</a>

    <p>A potential denial-of-service vulnerability was discovered in the
    read_ints function used when processing certain malformed Apple
    Property List files in binary format.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48565">CVE-2022-48565</a>

    <p>An XML External Entity (XXE) issue was discovered.  In order to
    avoid possible vulnerabilities, the plistlib module no longer
    accepts entity declarations in XML plist files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-48566">CVE-2022-48566</a>

    <p>Possible constant-time-defeating compiler optimisations were
    discovered in the accumulator variable in hmac.compare_digest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40217">CVE-2023-40217</a>

    <p>It was discovered that it might be possible to bypass some of the
    protections implemented by the TLS handshake in the ssl.SSLSocket
    class.  For example, unauthenticated data might be read by a program
    expecting data authenticated by client certificates.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.7.3-2+deb10u6.</p>

<p>We recommend that you upgrade your python3.7 packages.</p>

<p>For the detailed security status of python3.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python3.7">https://security-tracker.debian.org/tracker/python3.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3614.data"
# $Id: $
