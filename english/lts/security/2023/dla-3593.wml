<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in gerbv, a viewer for the Gerber
format for printed circuit board (PCB) design.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40393">CVE-2021-40393</a>

    <p>RS-274X format aperture macro variables out-of-bounds write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40394">CVE-2021-40394</a>

    <p>RS-274X aperture macro outline primitive integer overflow</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-4508">CVE-2023-4508</a>

    <p>Out-of-bounds memory access when referencing external files</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.7.0-1+deb10u3.</p>

<p>We recommend that you upgrade your gerbv packages.</p>

<p>For the detailed security status of gerbv please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gerbv">https://security-tracker.debian.org/tracker/gerbv</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3593.data"
# $Id: $
