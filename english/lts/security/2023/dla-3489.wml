<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security issue was discovered in MediaWiki, a website engine for
collaborative work, which could result in information disclosure when
SQLite files are created within a data directory that has weak permissions.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:1.31.16-1+deb10u5.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3489.data"
# $Id: $
