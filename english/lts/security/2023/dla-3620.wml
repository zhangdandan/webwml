<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been fixed in poppler,
a PDF rendering library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-23804">CVE-2020-23804</a>

    <p>Stack overflow in XRef::readXRefTable()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37050">CVE-2022-37050</a>

    <p>Crash in PDFDoc::savePageAs()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37051">CVE-2022-37051</a>

    <p>Crash in the pdfunite tool</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.71.0-5+deb10u3.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>For the detailed security status of poppler please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/poppler">https://security-tracker.debian.org/tracker/poppler</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3620.data"
# $Id: $
