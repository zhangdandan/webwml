<define-tag pagetitle>Debian Installer Bookworm RC 4 release</define-tag>
<define-tag release_date>2023-05-27</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the fourth release candidate of the installer for Debian 12
<q>Bookworm</q>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>cdebconf:
  <ul>
    <li>Improve banner display when the installer logo's width is smaller
    than the GTK window's width (<a href="https://bugs.debian.org/745359">#745359</a>): instead of scaling, expand
    the banner on the side(s) specified in the theme (<a href="https://bugs.debian.org/1036321">#1036321</a>).</li>
  </ul>
  </li>
  <li>debian-installer:
  <ul>
    <li>netboot/x86: fix colors in GRUB submenus (<a href="https://bugs.debian.org/1036771">#1036771</a>).</li>
    <li>netboot/x86: add /splash.png symlink to make GRUB load the
    splash screen (<a href="https://bugs.debian.org/1036215">#1036215</a>).</li>
  </ul>
  </li>
  <li>grub2:
  <ul>
    <li>When also installing to the removable media path, include the
    relevant mokmanager binary (<a href="https://bugs.debian.org/1034409">#1034409</a>).</li>
    <li>Allow multiple initrd parameters (<a href="https://bugs.debian.org/838177">#838177</a>, <a href="https://bugs.debian.org/820838">#820838</a>).</li>
  </ul>
  </li>
  <li>partman-auto-raid:
  <ul>
    <li>Add support for LVM-on-LUKS-on-RAID, via a new “crypto-lvm”
    keyword, similar to the existing “lvm” (<a href="https://bugs.debian.org/1036347">#1036347</a>).</li>
  </ul>
  </li>
  <li>partman-basicfilesystems:
  <ul>
    <li>Install dosfstools if we have a FAT ESP.</li>
  </ul>
  </li>
  <li>rootskel-gtk:
  <ul>
    <li>Add banner metadata (<a href="https://bugs.debian.org/745359">#745359</a>, <a href="https://bugs.debian.org/1036321">#1036321</a>).</li>
    <li>Align dark banner colors with dark/high-contrast theme.</li>
    <li>Fix document viewport, avoiding 1-pixel offsets.</li>
  </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>finish-install:
  <ul>
    <li>Add bochs/cirrus to the initramfs if detected (<a href="https://bugs.debian.org/1036788">#1036788</a>).</li>
  </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 42 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
