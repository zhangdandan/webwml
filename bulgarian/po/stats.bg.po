#
# Damyan Ivanov <dmn@debian.org>, 2011-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.61\n"
"PO-Revision-Date: 2020-12-17 14:42+0200\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 3.38.0\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Статистика на преводите на уеб сайта на Дебиан"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Има %d страници за превод."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Има %d байта за превод."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Има %d низа за превод."

#: ../../stattrans.pl:282 ../../stattrans.pl:495
msgid "Wrong translation version"
msgstr "Грешна версия на превода"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Преводът е твърде стар"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Оригиналният документ е променян след превода"

#: ../../stattrans.pl:290 ../../stattrans.pl:495
msgid "The original no longer exists"
msgstr "Оригиналният документ вече не съществува"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "не е наличен брой на попаденията"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "попадения"

#: ../../stattrans.pl:600 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Създадено с <transstatslink>"

#: ../../stattrans.pl:605
msgid "Translation summary for"
msgstr "Обобщена информация за превода на"

#: ../../stattrans.pl:608 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Няма превод"

#: ../../stattrans.pl:608 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Остарял"

#: ../../stattrans.pl:608
msgid "Translated"
msgstr "Преведено"

#: ../../stattrans.pl:608 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Обновен"

#: ../../stattrans.pl:609 ../../stattrans.pl:610 ../../stattrans.pl:611
#: ../../stattrans.pl:612
msgid "files"
msgstr "файла"

#: ../../stattrans.pl:615 ../../stattrans.pl:616 ../../stattrans.pl:617
#: ../../stattrans.pl:618
msgid "bytes"
msgstr "байта"

#: ../../stattrans.pl:625
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Забележка: списъкът на страниците е подреден по популярност. Поставете "
"мишката над името на страницата за да видите броя попадения."

#: ../../stattrans.pl:631
msgid "Outdated translations"
msgstr "Преводи, които имат нужда от обновяване"

#: ../../stattrans.pl:633 ../../stattrans.pl:686
msgid "File"
msgstr "Файл"

#: ../../stattrans.pl:635
msgid "Diff"
msgstr "Разлики"

#: ../../stattrans.pl:637
msgid "Comment"
msgstr "Бележка"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "Команден за Git"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Журнал на промените"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Превод"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Отговорник"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Статус"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Преводач"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Дата"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Общи страници без превод"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Общи страници без превод"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Новини без превод"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Новини без превод"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Страници за консултанти и потребители без превод"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Страници за консултанти и потребители без превод"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Международни страници без превод"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Международни страници без превод"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Преведени страници (обновени)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Преведени низове (файлове PO)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Статистика на преведените низове (PO)"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Мъгяви"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Без превод"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Общо"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Общо:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Преведени страници"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Статистика на преводите по брой страници"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Език"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Преводи"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Преведени страници (по размер)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Статистика на преводите по размер на страниците"

#~ msgid "Click to fetch diffstat data"
#~ msgstr "Извличане на данни за разликите"

#~ msgid "Colored diff"
#~ msgstr "Оцветени разлики"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Оцветени разлики"

#~ msgid "Created with"
#~ msgstr "Създаден с"

#~ msgid "Diffstat"
#~ msgstr "Обобщени разлики"

#~ msgid "Origin"
#~ msgstr "Оригинален документ"

#~ msgid "Unified diff"
#~ msgstr "Разлики в стандартен формат"
