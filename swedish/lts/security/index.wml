#use wml::debian::template title="LTS Säkerhetsinformation" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="28712bce73c493fd692778a4aadfe4c172ebaad9"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Hålla ditt Debian LTS-system säkert</toc-add-entry>

<p>Paketet <a href="https://packages.debian.org/stable/admin/unattended-upgrades">unattended-upgrades</a>
håller datorn uppdaterad med de senaste säkerhetsuppdateringarna (och andra) automatiskt.
<a href="https://wiki.debian.org/UnattendedUpgrades">Wikin</a> har
detaljerad information om hur man ställer in paketet.</p>

<p>För ytterligare information om säkerhetsfrågor i Debian, se
the <a href="../../security">Debians säkerhetsinformation</a>.</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">De senaste säkerhetsbulletinerna</toc-add-entry>

<p> Dessa webbsidor innehåller ett förkortat arkiv över de säkerhetsbulletiner
som postats på sändlistan <a 
href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a> list.

<p>(Kolla in det <a href="new.html#DLAS">nya listformatet</a>.)</p>


<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (endast titlar)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (sammanfattningar)" href="dla-long">
:#rss#}
<p>De senaste Debian LTS säkerhetsbulletinerna finns även tillgängliga i
<a href="dla">RDF format</a>. Vi erbbjuder också en
<a href="dla-long">andra fil</a> som innehåller första stycket i den 
aktuella bulletinen så att det går att se vad det handlar om.</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Även äldre säkerhetsbulletiner finns tillgängliga:
<:= get_past_sec_list(); :>

<!-- This section a copy from /security/dsa.wml. TODO: create and include file -->
<toc-add-entry name="infos">Källor för säkerhetsinformation</toc-add-entry>

<ul>
<li><a href="https://security-tracker.debian.org/">Debians Säkerhetsspårare</a>
primär källa för all säkerhetsrelaterad informatiom, sökalternativ</li>

<li><a href="https://security-tracker.debian.org/tracker/data/json">JSON-listan</a>
  innehåller CVE-beskrivningar, paketnamn, Debian-felrapportnummer, paketversioner med rättning, ingen DSA inkluderad
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DSA/list">DSA-listan</a>
   innehåller DSA inklusive datum, relaterade CVE:ers nummer, paketversioner med rättning
</li>

<li><a href="https://salsa.debian.org/security-tracker-team/security-tracker/-/raw/master/data/DLA/list">DLA-listan</a>
   innehåller DLA inklusive datum, relaterade CVE:ers nummer, paketversioner med rättning
</li>

<li><a href="https://lists.debian.org/debian-security-announce/">
DSA-tillkännagivanden</a></li>
<li><a href="https://lists.debian.org/debian-lts-announce/">
DLA-tillkännagivanden</a></li>

<li><a href="oval">Oval-filer</a></li>

<li>Slå upp en DSA (stora bokstäver är viktigt)<br>
t.ex. <tt>https://security-tracker.debian.org/tracker/DSA-3814</tt>
</li>

<li>Slå upp en DLA ( -1 är viktigt)<br>
t.ex. <tt>https://security-tracker.debian.org/tracker/DLA-867-1</tt>
</li>

<li>Slå upp en CVE<br>
t.ex. <tt>https://security-tracker.debian.org/tracker/CVE-2017-6827</tt>
</li>
</ul>

<ul>
<li> <a href="https://lts-team.pages.debian.net/wiki/FAQ">Debian LTS FAQ</a>. Din fråga kan mycket väl redan vara besvarad där!
<li>
<a href="https://lts-team.pages.debian.net/wiki/Contact">Kontaktinformation för Debian LTS-gruppen</a>
</ul>
