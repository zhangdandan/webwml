#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.1</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.1</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la première mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction apr "Déréférencement de tableau hors limites évité">
<correction atftp "Correction de dépassement de tampon [CVE-2021-41054]">
<correction automysqlbackup "Correction de plantage lors de l'utilisation de <q>LATEST=yes</q>">
<correction base-files "Mise à jour pour cette version">
<correction clamav "Nouvelle version amont stable ; correction d'erreurs de segmentation de clamdscan quand --fdpass et --multipass sont utilisés conjointement avec ExcludePath">
<correction cloud-init "Duplication d'includedir évitée dans /etc/sudoers">
<correction cyrus-imapd "Correction d'un problème de déni de service [CVE-2021-33582]">
<correction dazzdb "Correction d'une utilisation de mémoire après libération dans DBstats">
<correction debian-edu-config "debian-edu-ltsp-install : extension de la liste des exclusions relatives au serveur principal ; ajout de slapd et de xrdp-sesman à la liste des services masqués">
<correction debian-installer "Reconstruction avec proposed-updates ; mise à jour de l'ABI de Linux vers la version 5.10.0-9 ; utilisation des fichiers udeb issus de proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates ; utilisation des fichiers udeb issus de proposed-updates et de stable ; utilisation des fichiers de paquets avec la compression xz">
<correction detox "Correction de la gestion des grands fichiers">
<correction devscripts "bullseye-backports cible de l'option --bpo">
<correction dlt-viewer "Ajout des fichiers d'en-tête qdlt/qdlt*.h au paquet dev">
<correction dpdk "Nouvelle version amont stable">
<correction fetchmail "Correction d'erreur de segmentation et de régression de sécurité">
<correction flatpak "Nouvelle version amont stable ; plus d'héritage d'un réglage inhabituel de $XDG_RUNTIME_DIR dans le bac à sable">
<correction freeradius "Correction de plantage de thread et de configuration d'exemple">
<correction galera-3 "Nouvelle version amont stable">
<correction galera-4 "Nouvelle version amont stable ; résolution de conflits circulaires avec galera-3 en ne fournissant plus de paquet <q>galera</q> virtuel">
<correction glewlwyd "Correction d'un possible dépassement de tampon lors de la validation de signature FIDO2 dans l'enregistrement de webauthn [CVE-2021-40818]">
<correction glibc "Redémarrage d'openssh-server même s'il a été déconfiguré durant la mise à niveau ; correction du repli en mode texte quand debconf est inutilisable">
<correction gnome-maps "Nouvelle version amont stable ; correction d'un plantage lors du démarrage avec aérien comme dernier type de carte utilisé et qu'une définition de tuile non aérienne est trouvée ; parfois pas d'écriture de position de dernière vue cassée lors de la sortie ; correction de blocage lors de la sélection de marqueurs de route">
<correction gnome-shell "Nouvelle version amont stable ; correction de gel après l'annulation de (certains) dialogues de mode système ; correction de suggestions de mot dans les claviers virtuels ; correction de plantages">
<correction hdf5 "Ajustement des dépendances de paquets pour améliorer la mise à niveau des chemins à partir des versions plus anciennes">
<correction iotop-c "Gestion correcte des noms de processus en UTF-8">
<correction jailkit "Correction de la création de prisons qui ont besoin d'utiliser /dev ; correction de la vérification de la présence de bibliothèques">
<correction java-atk-wrapper "Utilisation également de dbus pour détecter l'activation de l'accessibilité">
<correction krb5 "Correction d'un plantage de déréférencement de pointeur NULL de KDC dans les requêtes FAST sans champ de serveur [CVE-2021-37750] ; correction de fuite de mémoire dans krb5_gss_inquire_cred">
<correction libavif "Utilisation de la libdir correcte dans le fichier pkgconfig de libavif.pc">
<correction libbluray "Passage à libasm incorporé ; la version issue de libasm-java est trop récente">
<correction libdatetime-timezone-perl "Nouvelle version amont stable ; mise à jour des règles DST pour les Samoa et la Jordanie ; confirmation de l'absence de seconde intercalaire le 31 décembre 2021">
<correction libslirp "Correction de plusieurs problèmes de dépassement de tampon [CVE-2021-3592 CVE-2021-3593 CVE-2021-3594 CVE-2021-3595]">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 9 ; [rt] mise à jour vers la version 5.10.65-rt53 ; [mipsel] bpf, mips : validation des positions de branche conditionnelle [CVE-2021-38300]">
<correction linux-signed-amd64 "Nouvelle version amont stable ; passage de l'ABI à la version 9 ; [rt] mise à jour vers la version 5.10.65-rt53 ; [mipsel] bpf, mips : validation des positions de branche conditionnelle [CVE-2021-38300]">
<correction linux-signed-arm64 "Nouvelle version amont stable ; passage de l'ABI à la version 9 ; [rt] mise à jour vers la version 5.10.65-rt53 ; [mipsel] bpf, mips : validation des positions de branche conditionnelle [CVE-2021-38300]">
<correction linux-signed-i386 "Nouvelle version amont stable ; passage de l'ABI à la version 9 ; [rt] mise à jour vers la version 5.10.65-rt53 ; [mipsel] bpf, mips : validation des positions de branche conditionnelle [CVE-2021-38300]">
<correction mariadb-10.5 "Nouvelle version amont stable ; corrections de sécurité [CVE-2021-2372 CVE-2021-2389]">
<correction mbrola "Correction de la détection de fin de fichier">
<correction modsecurity-crs "Correction d'un problème de contournement de corps de requête [CVE-2021-35368]">
<correction mtr "Correction d'une régression dans une sortie JSON">
<correction mutter "Nouvelle version amont stable ; kms : amélioration de la gestion des modes vidéo courants qui pourraient dépasser la bande passante possible ; assurance d'une taille de texture de fenêtre valable après une modification de fenêtre">
<correction nautilus "Ouverture évitée de plusieurs fichiers sélectionnés dans de multiples instances de l'application ; pas de sauvegarde de la taille et de la position des fenêtres quand elles sont tuilées ; correction de certaines fuites de mémoire ; mise à jour des traductions">
<correction node-ansi-regex "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2021-3807]">
<correction node-axios "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2021-3749]">
<correction node-object-path "Correction de problèmes de pollution de prototype [CVE-2021-23434 CVE-2021-3805]">
<correction node-prismjs "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2021-3801]">
<correction node-set-value "Correction d'une pollution de prototype [CVE-2021-23440]">
<correction node-tar "Suppression des chemins qui ne sont pas des répertoires du cache de répertoires [CVE-2021-32803] ; retrait des chemins absolus de façon plus complète [CVE-2021-32804]">
<correction osmcoastline "Correction de projections autres que WGS84">
<correction osmpbf "Reconstruction avec protobuf 3.12.4">
<correction pam "Correction d'erreur de syntaxe dans libpam0g.postinst quand une unité systemd échoue">
<correction perl "Mise à jour de sécurité ; correction d'une fuite de mémoire d'expression rationnelle">
<correction pglogical "Mise à jour de l'instantané de PostgreSQL 13.4 gérant les corrections">
<correction pmdk "Correction de l'absence de barrière après un appel memcpy non temporaire">
<correction postgresql-13 "Nouvelle version amont stable ; correction d'une mauvaise programmation de l'application répétée d'une étape de projection [CVE-2021-3677] ; interdiction plus complète des renégociations SSL">
<correction proftpd-dfsg "Correction de <q>mod_radius divulgue des contenus de la mémoire au serveur radius</q> et de <q>la connexion sftp échoue avec</q> MAC corrompue sur l'entrée de "" ; échappement évité de texte SQL déjà protégé">
<correction pyx3 "Correction d'un problème d'alignement horizontal de fonte avec texlive 2020">
<correction reportbug "Mise à jour des noms de suite après la publication de Bullseye">
<correction request-tracker4 "Correction d'un problème d'attaque temporelle par canal auxiliaire pour la connexion [CVE-2021-38562]">
<correction rhonabwy "Correction du calcul de l'étiquette JWE CBC et de la vérification de la  signature JWS alg:none">
<correction rpki-trust-anchors "Ajout de l'URL HTTPS au LACNIC TAL">
<correction rsync "Réintégration de --copy-devices ; correction de regression dans --delay-updates ; correction de cas extrêmes dans --mkpath ; correction de rsync-ssl ; correction de --sparce et --inplace ; mise à jour des options disponibles pour rrsync ; corrections de la documentation">
<correction ruby-rqrcode-rails3 "Correction de la compatibilité avec ruby-rqrcode 1.0">
<correction sabnzbdplus "Échappement de répertoire évité dans la fonction de renommage [CVE-2021-29488]">
<correction shellcheck "Correction du rendu des options longues dans les pages de manuel">
<correction shiro "Correction de problèmes de contournement d'authentification [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510] ; mise à jour du correctif de la compatibilité avec le cadriciel Spring ; prise en charge de Guice 4">
<correction speech-dispatcher "Correction du réglage du nom de voix pour le module générique">
<correction telegram-desktop "Plantage évité lorsque la suppression automatique est activée">
<correction termshark "Inclusion de thèmes dans le paquet">
<correction tmux "Correction d'une situation de compétition qui a pour conséquence que la configuration n'est pas chargée si plusieurs clients interagissent avec le serveur lors de son initialisation">
<correction txt2man "Correction de la régression dans la gestion de <q>display blocks</q>">
<correction tzdata "Mise à jour des règles DST pour les Samoa et la Jordanie ; confirmation de l'absence de seconde intercalaire le 31 décembre 2021">
<correction ublock-origin "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2021-36773]">
<correction ulfius "Initialisation assurée de la mémoire avant utilisation [CVE-2021-40540]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2021 4959 thunderbird>
<dsa 2021 4960 haproxy>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4965 libssh>
<dsa 2021 4966 gpac>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4968 haproxy>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4972 ghostscript>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4976 wpewebkit>
<dsa 2021 4977 xen>
<dsa 2021 4978 linux-signed-amd64>
<dsa 2021 4978 linux-signed-arm64>
<dsa 2021 4978 linux-signed-i386>
<dsa 2021 4978 linux>
<dsa 2021 4979 mediawiki>
</table>

<p>
Durant les étapes finales du gel de Bullseye, certaines mises à jour ont été
publiées au moyen de 
<a href="https://security.debian.org/">l'archive de sécurité</a> mais sans une
DSA d'accompagnement. Ces mises à jour sont détaillées ci-dessous.
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction apache2 "Correction d'injection d'une ligne de requête HTTP2 mod_proxy [CVE-2021-33193]">
<correction btrbk "Correction d'un problème d'exécution de code arbitraire [CVE-2021-38173]">
<correction c-ares "Correction de l'absence de validation d'entrée sur les noms d'hôte renvoyés par les serveurs DNS [CVE-2021-3672]">
<correction exiv2 "Correction de problèmes de dépassement de tampon [CVE-2021-29457 CVE-2021-31292]">
<correction firefox-esr "Nouvelle version amont stable [CVE-2021-29980 CVE-2021-29984 CVE-2021-29985 CVE-2021-29986 CVE-2021-29988 CVE-2021-29989]">
<correction libencode-perl "Encode : atténuation de la pollution d'@INC lors du chargement de ConfigLocal [CVE-2021-36770]">
<correction libspf2 "spf_compile.c : rectification de la taille de ds_avail [CVE-2021-20314] ; correction du modificateur de macro <q>reverse</q>">
<correction lynx "Correction d'une fuite d'identifiant si SNI était utilisé conjointement avec une URL contenant l'identifiant [CVE-2021-38165]">
<correction nodejs "Nouvelle version amont stable ; correction d'un problème d'utilisation de mémoire après libération [CVE-2021-22930]">
<correction tomcat9 "Correction d'un problème de contournement d'authentification [CVE-2021-30640] et d'un problème de dissimulation de requête [CVE-2021-33037]">
<correction xmlgraphics-commons "Correction d'un problème de contrefaçon de requête côté serveur [CVE-2020-11988]">
</table>


<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contact Information</h2>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
