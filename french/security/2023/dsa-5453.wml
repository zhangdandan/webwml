#use wml::debian::translation-check translation="cf4a38312fe3e85dc51ffb5aa69c7f899ea43186" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2156">CVE-2023-2156</a>

<p>Un défaut dans le traitement du protocole RPL peut permettre à un
attaquant distant non authentifié de provoquer un déni de service si RPL
est activé (ce qui n'est pas le cas par défaut dans Debian).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31248">CVE-2023-31248</a>

<p>Mingi Cho a découvert un défaut d'utilisation de mémoire après
libération dans l'implémentation de nf_tables de Netfilter lors de
l'utilisation de nft_chain_lookup_byid, ce qui pouvait avoir pour
conséquence une élévation de privilèges locale pour un utilisateur doté de
la capacité CAP_NET_ADMIN dans n'importe quel espace de noms
utilisateur ou réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-35001">CVE-2023-35001</a>

<p>Tanguy DUBROCA a découvert un défaut de lecture et d'écriture hors
limites dans l'implémentation de Netfilter nf_tables lors du traitement
d'une expression nft_byteorder, ce qui pouvait avoir pour conséquence une
élévation de privilèges locale pour un utilisateur doté de la capacité
CAP_NET_ADMIN dans n'importe quel espace de noms utilisateur ou réseau.</p></li>

</ul>

<p>Pour la distribution oldstable (Bullseye), ces problèmes ont été
corrigés dans la version 5.10.179-2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5453.data"
# $Id: $
