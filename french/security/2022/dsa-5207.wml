#use wml::debian::translation-check translation="0413fa50844efcfa46bf7f8f7809d825fd42fffa" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2585">CVE-2022-2585</a>

<p>Un défaut d'utilisation de la mémoire après libération dans
l'implémentation des temporisations CPU POSIX peut avoir pour conséquences
un déni de service ou une élévation locale de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2586">CVE-2022-2586</a>

<p>Une utilisation de mémoire après libération dans le sous-système
Netfilter peut avoir pour conséquence une élévation locale de privilèges
pour un utilisateur doté de la capacité CAP_NET_ADMIN dans n'importe quel
espace de noms utilisateur ou réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2588">CVE-2022-2588</a>

<p>Zhenpeng Lin a découvert un défaut d'utilisation de mémoire après
libération dans l'implémentation du filtre cls_route qui peut avoir pour
conséquence une élévation locale de privilèges pour un utilisateur doté de
la capacité CAP_NET_ADMIN dans n'importe quel espace de noms utilisateur ou
réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26373">CVE-2022-26373</a>

<p>Sur certains processeurs dotés des capacités <q>Enhanced Indirect Branch
Restricted Speculation</q> (eIBRS) d'Intel, il y a des exceptions aux
propriétés documentées dans certaines situations qui peuvent avoir pour
conséquence la divulgation d'informations.</p>

<p>L'explication du problème par Intel peut être trouvée sur la page
<url "https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29900">CVE-2022-29900</a>

<p>Johannes Wikner et Kaveh Razavi ont signalé que pour les processeurs
AMD ou Hygon des prédictions de branchement avec un mauvais apprentissage
pour des instructions <q>return</q> peuvent permettre l'exécution de code
spéculatif arbitraire dans certaines conditions dépendant de la
micro-architecture.</p>

<p>Une liste des types de processeurs AMD affectés se trouve sur la page
<url "https://www.amd.com/en/corporate/product-security/bulletin/amd-sb-1037">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29901">CVE-2022-29901</a>

<p>Johannes Wikner et Kaveh Razavi ont signalé que pour les processeurs
Intel (Intel Core génération 6, 7 et 8), les protections contre les
attaques par injection de cible de branchement spéculatif étaient
insuffisantes dans certaines circonstances, ce qui peut permettre
l'exécution de code spéculatif arbitraire dans certaines conditions
dépendant de la micro-architecture.</p>

<p>D'avantage d'informations peuvent être trouvées sur la page
<url "https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36879">CVE-2022-36879</a>

<p>Un défaut a été découvert dans xfrm_expand_policies dans le sous-système
xfrm qui peut faire qu'un compte de références soit supprimé deux fois.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36946">CVE-2022-36946</a>

<p>Domingo Dirutigliano et Nicola Guerrera ont signalé un défaut de
corruption de mémoire dans le sous-système Netfilter qui peut avoir pour
conséquence un déni de service.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.136-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5207.data"
# $Id: $
