#use wml::debian::translation-check translation="a7ce2c61187bfccd9c243ec7dfcb09eb585096fc" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur
web WebKitGTK :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42799">CVE-2022-42799</a>

<p>Jihwan Kim et Dohyun Lee ont découvert que la visite d'un site
malveillant pouvait conduire à une usurpation de l'interface utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42823">CVE-2022-42823</a>

<p>Dohyun Lee a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42824">CVE-2022-42824</a>

<p>Abdulrahman Alqabandi, Ryan Shin et Dohyun Lee ont découvert que le
traitement d'un contenu web contrefait pouvait divulguer des informations
sensibles de l'utilisateur.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.38.2-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5273.data"
# $Id: $
