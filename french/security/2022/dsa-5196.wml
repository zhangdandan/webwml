#use wml::debian::translation-check translation="f9b3401db5e222ddd741acdd79fc20ed59ed8961" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités sécurité ont été découvertes dans libpgjava, le
pilote JDBC officiel de PostgreSQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13692">CVE-2020-13692</a>

<p>Une faiblesse d'entité externe XML (XXE) a été découverte dans le
pilote JDBC de PostgreSQL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21724">CVE-2022-21724</a>

<p>Le pilote JDBC ne vérifiait si certaines classes implémentaient
l'interface attendue avant d'instancier la classe. Cela peut conduire à
l'exécution de code à l'aide de classes arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26520">CVE-2022-26520</a>

<p>Un attaquant qui contrôle l'URL ou les propriétés de jdbc peut appeler
java.util.logging.FileHandler pour écrire dans des fichiers arbitraires au
moyen des propriétés de connexion loggerFile et loggerLevel.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 42.2.5-2+deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 42.2.15-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libpgjava.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libpgjava, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libpgjava">\
https://security-tracker.debian.org/tracker/libpgjava</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5196.data"
# $Id: $
