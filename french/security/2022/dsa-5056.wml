#use wml::debian::translation-check translation="f8545f462fdd05fa94ece499d3a78fc01e46f8ed" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Zhuowei Zhang a découvert un bogue dans le code d'authentification EAP
de strongSwan, une suite IKE/IPsec, qui pouvait permettre le contournement
de l'authentification du client et même du serveur dans certains scénarios,
ou pouvait conduire à une attaque par déni de service.</p>

<p>Lors de l'utilisation de l'authentification EAP (RFC 3748), la réussite
de l'authentification est indiquée par un message EAP-Success envoyé par le
serveur au client. Le code du client EAP de strongSwan traitait
incorrectement des messages EAP-Success précoces, soit en plantant le démon
IKE, soit en concluant prématurément la méthode EAP.</p>

<p>Le résultat final dépend de la configuration utilisée. Plus de
précisions sont disponibles dans l'annonce amont à l'adresse
<a href="https://www.strongswan.org/blog/2022/01/24/strongswan-vulnerability-(cve-2021-45079).html">\
https://www.strongswan.org/blog/2022/01/24/strongswan-vulnerability-(cve-2021-45079).html</a>.</p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 5.7.2-1+deb10u2.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 5.9.1-1+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets strongswan.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de strongswan, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5056.data"
# $Id: $
