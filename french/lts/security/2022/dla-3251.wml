#use wml::debian::translation-check translation="50c7cbecdead9f5149bb098eafe73c8134ffd7ed" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>ZeddYu Lu a découvert que le client FTP d’Apache Commons Net, une API cliente
en Java pour les protocoles basiques d’Internet, acceptait l’hôte de la réponse
PASV par défaut. Un serveur malveillant pouvait rediriger le code Commons Net
pour utiliser un hôte différent, mais l’utilisateur devait d’abord se connecter
au serveur malveillant. Cela pouvait conduire à une fuite d’informations sur les
services en cours d’exécution sur le réseau privé du client.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 3.6-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libcommons-net-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libcommons-net-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libcommons-net-java">\
https://security-tracker.debian.org/tracker/libcommons-net-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3251.data"
# $Id: $
