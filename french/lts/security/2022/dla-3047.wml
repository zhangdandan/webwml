#use wml::debian::translation-check translation="647449219fb46719543d1f71aa60e782e42d11eb" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le paquet Debian d'Avahi, un cadriciel pour le <q>Multicast DNS Service
Discovery</q>, exécutait le script avahi-daemon-check-dns.sh avec les
privilèges du superutilisateur ce qui pouvait permettre à un attaquant
local de provoquer un déni de service ou de créer des fichiers vides
arbitraires à l'aide d'un lien symbolique sur des fichiers sous
/var/run/avahi-daemon. Le script est maintenant exécuté avec les privilèges
de l'utilisateur et du groupe avahi et requiert sudo afin de réaliser cela.</p>

<p>Le script susmentionné a été retiré à partir de Debian 10 <q>Buster</q>.
Le palliatif ne peut pas être implémenté pour Debian 9 <q>Stretch</q>
parce que libnss-mdns 0.10 ne fournit pas la fonction requise pour le
remplacer.</p>

<p>En outre, il a été découvert
(<a href="https://security-tracker.debian.org/tracker/CVE-2021-3468">CVE-2021-3468</a>)
que l'événement utilisé pour signaler l'interruption de la connexion du
client sur le socket Unix d'avahi n'est pas correctement géré dans la
fonction client_work, permettant à un attaquant local de déclencher une
boucle infinie.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.6.32-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets avahi.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de avahi, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/avahi">\
https://security-tracker.debian.org/tracker/avahi</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3047.data"
# $Id: $
