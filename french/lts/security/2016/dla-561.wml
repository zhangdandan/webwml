#use wml::debian::translation-check translation="fce40adff1381ed16a3e5ebae7ad1ff8fcbbb739" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans uClibc, une
implémentation de la bibliothèque C standard, beaucoup plus petite que
glibc, ce qui la rend utile pour les systèmes embarqués.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2224">CVE-2016-2224</a>

<p>Correction d'un déni de service potentiel à l'aide d'une réponse DNS
contrefaite pour l'occasion qui pourrait provoquer une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2225">CVE-2016-2225</a>

<p>Correction d'un déni de service potentiel à l'aide d'un paquet
contrefait pour l'occasion qui fera que l'analyseur dans libc/inet/resolv.c
s'arrêtera prématurément.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6264">CVE-2016-6264</a>

<p>Il a été découvert que l'instruction <q>BLT</q> dans la vérification
des valeurs signées de libc/string/arm/memset.S. Si le paramètre de memset
est négatif, alors, la valeur ajoutée au PC sera grande. Un attaquant qui
contrôle le paramètre de largeur de memset peut aussi contrôler la valeur
du registre PC.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.9.32-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets uclibc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-561.data"
# $Id: $
