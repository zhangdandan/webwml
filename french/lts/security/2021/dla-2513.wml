#use wml::debian::translation-check translation="b0c7c5fc853e5e375f27441ed8f6e7c48f63c246" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité mémoire affectant le protocole RPC ont été
corrigés dans p11-kit, une bibliothèque fournissant un moyen de charger et
d'énumérer les modules PKCS#11.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29361">CVE-2020-29361</a>

<p>Plusieurs dépassement d'entiers</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29362">CVE-2020-29362</a>

<p>Lecture hors limites de tampon de tas</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.23.3-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets p11-kit.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de p11-kit, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/p11-kit">https://security-tracker.debian.org/tracker/p11-kit</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2513.data"
# $Id: $
