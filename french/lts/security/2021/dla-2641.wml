#use wml::debian::translation-check translation="79fd7da21476317a94f6c3bdf53e504820f40443" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans les greffons pour
l'environnement multimédia GStreamer. Cela peut avoir pour conséquence un déni
de service ou éventuellement l'exécution de code arbitraire lors de l'ouverture
d'un fichier média mal formé.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.10.4-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gst-plugins-base1.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gst-plugins-base1.0, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gst-plugins-base1.0">\
https://security-tracker.debian.org/tracker/gst-plugins-base1.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2641.data"
# $Id: $
