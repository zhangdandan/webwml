#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ming.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9988">CVE-2017-9988</a>

<p>Déréférencement de pointeur NULL dans la fonction readEncUInt30 (util/read.c)
dans Ming <= 0.4.8. Cela permet à des attaquants de provoquer un déni de service
à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9989">CVE-2017-9989</a>

<p>Déréférencement de pointeur NULL dans la fonction outputABC_STRING_INFO
(util/outputtxt.c) dans Ming <= 0.4.8. Cela permet à des attaquants de provoquer
un déni de service à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11733">CVE-2017-11733</a>

<p>Déréférencement de pointeur NULL dans la fonction stackswap (util/decompile.c)
dans Ming <= 0.4.8. Cela permet à des attaquants de provoquer un déni de service
à l'aide d'un fichier contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:0.4.4-1.1+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ming.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1176.data"
# $Id: $
