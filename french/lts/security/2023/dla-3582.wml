#use wml::debian::translation-check translation="2ae73f59a1b57da280f498c28a4e2fd533a74a9b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans ghostscript, un interpréteur
pour le langage PostScript PDF. Elles permettaient à des attaquants distants de
provoquer un déni de service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21710">CVE-2020-21710</a>

<p>Division par zéro provoquée par une résolution personnalisée trop basse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21890">CVE-2020-21890</a>

<p>Vulnérabilité de dépassement de tampon dans la fonction clj_media_size.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 9.27~dfsg-2+deb10u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ghostscript.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ghostscript,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ghostscript">\
https://security-tracker.debian.org/tracker/ghostscript</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3582.data"
# $Id: $
