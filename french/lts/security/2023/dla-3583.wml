#use wml::debian::translation-check translation="ee048ee73c3edbacc47b8d471be304dea50de56c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été trouvées dans GLib, une
bibliothèque généraliste d’utilitaires employée par des projets tels que GTK+, 
GIMP et GNOME.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-29499">CVE-2023-29499</a>

<p>La désérialisation de GVariant échouait à valider le fait que l’entrée était
conforme au format attendu, conduisant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32611">CVE-2023-32611</a>

<p>La désérialisation de GVariant était vulnérable à un problème de
ralentissement quand une GVariant contrefaite pouvait provoquer un traitement
excessif, conduisant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32665">CVE-2023-32665</a>

<p>La désérialisation de GVariant était vulnérable à un problème d’augmentation
exponentielle quand une GVariant contrefaite pouvait provoquer un traitement
excessif, conduisant à un déni de service.</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.58.3-2+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets glib2.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de glib2.0,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/glib2.0">\
https://security-tracker.debian.org/tracker/glib2.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3583.data"
# $Id: $
