#use wml::debian::translation-check translation="4993b4ccf079ab61f7fad315680d9ac9aeeb1d21" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle d’exécution
de code à distance dans phppgadmin, un outil d’administration basé sur le web
pour le serveur de base de données PostgreSQL. Ce problème concernait la
désérialisation de données non fiables, qui pouvait conduire à une exécution de
code à distance parce que des données contrôlées par l’utilisateur étaient
passées directement à la fonction PHP <code>unserialize()</code>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-40619">CVE-2023-40619</a>

<p>phpPgAdmin, versions 7.14.4 et précédentes, était vulnérable à une
désérialisation de données non fiables, qui pouvait conduire à une exécution de
code à distance parce des données contrôlées par l’utilisateur étaient passées
directement à la fonction PHP <q>unserialize()</q> dans plusieurs endroits.
Un exemple est la fonctionnalité pour gérer des tables dans <q>tables.php</q> où
le paramètre POST <q>ma[]</q> est désérialisé.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5.1+ds-4+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phppgadmin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3644.data"
# $Id: $
