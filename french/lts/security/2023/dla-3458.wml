#use wml::debian::translation-check translation="4769db5536eadadd691094e929778dd36e68cfa7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Niels Dossche et Tim Düsterhus ont découvert que l’implémentation de PHP de
l’authentification Digest HTTP de SOAP ne recherchait pas les échecs, ce qui
pouvait aboutir à une fuite d'informations de pile. De plus, le code utilisait
un nombre d’octets aléatoires insuffisant.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 7.3.31-1~deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php7.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php7.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php7.3">\
https://security-tracker.debian.org/tracker/php7.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3458.data"
# $Id: $
