#use wml::debian::translation-check translation="2994ab407543f2e369424b6e090c80963a6acf6a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un couple de problèmes de sécurité a été découvert dans ruby2.5,
l’interpréteur de Ruby.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33621">CVE-2021-33621</a>

<p>Hiroshi Tokumaru a découvert que Ruby ne gérait pas correctement certaines
entrées utilisateur pour les applications qui généraient des réponses HTTP en
utilisant le gem cgi. Un attaquant pouvait éventuellement utiliser ce problème
pour modifier de manière malveillante la réponse qu’un utilisateur devait
recevoir d’une application vulnérable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28739">CVE-2022-28739</a>

<p>Il a été découvert que Ruby gérait incorrectement certaines entrées. Un
attaquant pouvait éventuellement utiliser ce problème pour exposer des
informations sensibles.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés, ainsi que les
régressions causées par la dernière mise à jour de Ruby dans la
version 2.5.5-3+deb10u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby2.5.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby2.5,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby2.5">\
https://security-tracker.debian.org/tracker/ruby2.5</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3450.data"
# $Id: $
