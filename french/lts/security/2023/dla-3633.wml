#use wml::debian::translation-check translation="3ac64a623a264ef38927f785575e50920110e0f3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans les greffons de
l'environnement multimédia GStreamer, ses codecs et ses démultiplexeurs, qui
peuvent avoir pour conséquences un déni de service ou l'exécution de code
arbitraire lors de l'ouverture d'un fichier média mal formé.</p>


<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.14.4-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gst-plugins-bad1.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gst-plugins-bad1.0,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gst-plugins-bad1.0">\
https://security-tracker.debian.org/tracker/gst-plugins-bad1.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3633.data"
# $Id: $
