#use wml::debian::template title=""Overzetting naar PowerPC (PReP)" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/powerpc/inst/menu.inc"
#use wml::debian::translation-check translation="baca2cc9eb45158bf723feec7aa48e19ee745253"

<h1>Debian GNU/Linux PowerPC PReP-pagina..</h1>

<h3>Intro</h3>
<p>

IBM en Motorola maken of maakten allebei machines met PReP-architectuur. Op dit moment is de PReP-ondersteuning alleen getest op oudere PowerStack(tm)-systemen (Blackhawk, Comet, Utah-moederbord) van Motorola en de huidige ingebedde oplossing uit de PowerPlus(tm)-familie. Hieronder vallen de MTX, MTX+, MVME2300(sc)/2400/2600/2700/3600/4600 en MCP(n)750. IBM produceert verschillende desktopwerkstations die compatibel zijn met PowerPC PReP. Hieronder vallen de RS/6000 40P, 43P, 830, 850, 860, 6015 en 6030.

</p>

<h3>Bekende problemen</h3>
<p>
MTX+ (MTX-systeem met 7 PCI-sleuven) kan niet opstarten met de 2.2-kernel. Deze blijft hangen bij de initialisatie van het IDE-stuurprogramma. Dit is een bekend probleem en zal worden opgelost in een komende kernelpatch.
</p>
<p>
Bij alle Motorola PowerPlus-systemen is de IDE-ondersteuning in de kernel defect. Dit probleem is geïdentificeerd en opgelost. De wijzigingen zullen binnenkort beschikbaar zijn als een Debian-kernelpatchpakket en zullen in de kernelbroncode worden opgenomen.
</p>

<h3>Firmware</h3>
<p>
Afhankelijk van de leeftijd en/of de fabrikant van uw PReP-systeem beschikt u over PPCBUG (Motorola), Open Firmware (IBM of Motorola) of IBM's PReP-firmware. De opstartopdrachten zijn iets anders, maar al deze systemen implementeren de PReP-standaard, zodat dezelfde kernel op alle platformen kan worden opgestart. Een volledige uitleg over firmware-opdrachten valt buiten het bestek van dit document, maar waar nodig zal enige informatie worden verstrekt.

</p>

<h3>Een installatie opzetten</h3>

<h4>Installatie met diskettes</h4>

<p>
Schrijf <code>boot1440.bin</code>, <code>root1440.bin</code>,
<code>resc1440.bin</code>, <code>drv14-*.bin</code> en
images naar diskettes. <code>dd</code> kan
gebruikt worden op een Linux/Unix-systeem en <code>rawrite</code> kan gebruikt
worden op een DOS/Windows-systeem.
</p>

<h4>Installatie over het netwerk</H4>
<p>

Plaats de bestanden <code>resc1440.bin</code> en <code>drivers.tgz</code>
op een geëxporteerd NFS-bestandssysteem op uw NFS-server. Het beste kunt u
de bestanden als volgt plaatsen:
<code>/[aankoppelpunt]/debian/[installatiebestanden]</code> .
</p>

<p>
Plaats het bestand <code>boot.bin</code> in de tftp-map op uw TFTP-server.
</p>

<h4>Consoleopties</h4>
<p>
Op Motorola PPCBUG-systemen kunt u opstarten en installeren via een VGA-console of een seriële console. PPCBUG en Linux vallen terug naar de seriële consolemodus als er geen VGA of toetsenbord wordt gedetecteerd.
</p>

<p>
Om de installatie op een seriële console goed te kunnen bekijken is een terminalemulator met een aantal kwaliteiten noodzakelijk. Om de installatie in kleur te kunnen zien, is het noodzakelijk om een terminalemulator te hebben met ondersteuning voor ANSI-kleur. De standaard xterm in kleur die bij de meeste Linux-distributies wordt geleverd, is voldoende, net als de meeste terminalprogramma's op Windows (bijvoorbeeld <a
href="http://www.chiark.greenend.org.uk/~sgtatham/putty/">PuTTY</a>). Om de lijnlettertekens te kunnen zien waarmee de GUI-vakken gevormd worden, is het essentieel dat een lettertype wordt gebruikt dat de uitgebreide lijnlettertekens van IBM bevat. Onder Linux kan het lettertype "linux8x8" worden gebruikt en onder Windows kan men het lettertype MSLineDraw selecteren.
</p>


<h3>Opstarten</h3>
<dl>
<dt> PPCBUG
<dd> Gebruik het commando "ENV" en schakel de optie "Network PReP boot enable" in.
<dd> Gebruik het commando "NIOT" om het IP-adres van de client, de server en facultatief de gateway te configureren die zullen worden gebruikt om de kernel via tftp op te halen. Als het om een gloednieuw bord gaat, zorg er dan voor dat u <tt>SET <var>&lt;date/time&gt;</var></tt> instelt om de hardwareklok te starten, anders zal het netwerk en andere dingen niet werken.
<dt> Start het image met
<dd> PPCBUG&gt;NBO 0 0 ,,, bootprep.bin
<dt> OpenFirmware
<dt> Gebruik het volgende commando om over tftp een PReP-image op te starten:
<dd> &gt;boot &lsaquo;server_ipadr&rsaquo;,&lsaquo;bestand&rsaquo;,&lsaquo;client_ipadr&rsaquo;
</dl>



<h2>Installatie van Debian</h2>
<ol>
<li> Het toetsenbord configureren
<p>
Als u zich aan een seriële console bevindt, wordt de stap voor het configureren van het toetsenbord overgeslagen.
</p>

<li> De harde schijf indelen
<p>
Maak een primaire partitie van 2MB en wijzig het type in PPC PReP boot (type 41). OF PReP-machines kunnen problemen hebben met het opstarten als de PPC PReP-opstartpartitie ergens anders staat dan op de primaire partitie 1. Hoewel alle Motorola PPCBUG-systemen de PPC PReP-opstartpartitie op elke primaire partitie kunnen hebben, is het algemeen gebruikelijk om de PPC PReP-opstartpartitie op sda1 of hda1 te zetten.
</p>
<p>
Nadat de PPC PReP opstartpartitie is aangemaakt, volgt u de normale Linux-conventies voor schijfindeling. Er moet ten minste een root- en swappartitie worden aangemaakt en deze kunnen op primaire of logische partities staan.
</p>

<li> De rescue- en stuurprogrammadiskettes installeren
<p>
Installeren vanaf diskette is eenvoudig, plaats de rescue-diskette en de stuurprogrammadiskette in het station als daarom wordt gevraagd.
</p>
<p>
Voor een netwerkinstallatie kiest u de NFS-optie, waarna u wordt gevraagd het netwerk te configureren. Wanneer wordt gevraagd welke NFS-server moet worden gebruikt, voert u de NFS-server en -map in die u eerder hebt ingesteld. Kies de standaardinstellingen voor de rest van de vragen.
</p>

<li> De stuurprogrammamodules configureren
<p>
 Voorlopig worden belangrijke modules in de kernel ingebouwd, dus kies gewoon "Exit".
</p>

<li> Het basissysteem installeren
<p>
Als een installatie met diskettes werd geselecteerd, plaatst u gewoon de basisdiskettes in het station wanneer er om wordt gevraagd. Als een NFS-installatie is geselecteerd, voert u de NFS-server en -map in waar het basissysteem zich bevindt en kiest u de standaardinstellingen om het Debian-basissysteem te installeren.
</p>

<li> Opstartbaar maken vanaf de harde schijf
<p>
Maakt het systeem opstartbaar vanaf de PPC PReP-opstartpartitie die eerder is gemaakt. Als die partitie ontbreekt, wordt er een foutmelding weergegeven.
</p>

<li> Een opstartdiskette maken
<p>
Schrijft een opstartbaar image naar een diskette in het diskettestation.
</p>

</ol>

<h2>Het Debian basissysteem opstarten</h2>
<p>
Als u een type 41 PReP opstartpartitie hebt geconfigureerd en het installatieprogramma het systeem opstartbaar hebt laten maken vanaf de harde schijf, dan kunt u gewoon een firmware-opstartopdracht geven om het systeem vanaf de harde schijf te laten opstarten (PPCBUG en OF hebben allebei een optie automatisch opstarten die u ook kunt inschakelen).
</p>
<p>
Opmerking: Om de kernel te dwingen om de hoofdmap (root) op de juiste partitie te situeren, kan het nodig zijn om naar de PReP Globale Omgevingsvariabele "bootargs" te schrijven. In het geval van een Motorola PReP installatie die de hoofdmap standaard op sda1 situeert en waarbij het root-bestandssysteem in werkelijkheid op sda2 staat, moet <tt>bootargs=/dev/sda2</tt> ingesteld worden.
</p>

<p>PPCBUG</p>

<dl>
<dt>De parameter bootargs instellen
<dd>PPCBUG&gt;GEVEDIT bootargs
<dd>PPCBUG&gt;bootargs=root=/dev/sda2
<dt>Een opstartprocedure uitvoeren (gaat ervan uit dat de SCSI-schijf op controller 0 staat, SCSI ID 0):
<dd>PPCBUG&gt;PBOOT 0
<dt>Een opstartprocedure uitvoeren (gaat ervan uit dat de SCSI-schijf op controller 0 staat, SCSI ID x)
<dd>PPCBUG&gt;PBOOT 0 x0
</dl>
<dl>
<dt>Openfirmware
<dt>Een opstartprocedure uitvoeren (voor geïnstalleerde IDE/SCSI-schijf met alias disk0)
<dd>&gt;boot disk0
<dt>Een opstartprocedure uitvoeren (voor geïnstalleerde IDE/SCSI-schijf met alias hdisk0)
<dd>&gt;boot hdisk0
</dl>
<p>
Nu start de kernel op vanaf de harde schijf.
</p>

<hr>
Stuur suggesties/klachten/problemen met de installatie op PReP en deze documentatie per e-mail naar <a href="mailto:porter@debian.org">Matt Porter</a>
