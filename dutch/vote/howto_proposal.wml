#use wml::debian::translation-check translation="beb188a1e37dd39a8004ebd9faddc4b43118a184"
<define-tag pagetitle>Procedures voor het indienen van een voorstel van algemene resolutie of van een amendement</define-tag>
#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true" NOHOMELINK="true"
#use wml::debian::votebar

    <h1><pagetitle></h1>

    <p>Onder de volgende afdelingen van de statuten</p>
    <div style="border: solid; padding: 1ex">
      <blockquote>
        <ul>
          <li>
            4. De ontwikkelaars door middel van een algemene resolutie of een verkiezing
            <ul>
              <li>
               2. Procedure
                <ul>
                  <li>
                   5. Voorstellen, steunbetuigingen, amendementen, oproepen tot stemming en andere formele handelingen worden aangekondigd op een openbaar leesbare elektronische mailinglijst die wordt aangewezen door de gemachtigde(n) van de projectleider; elke ontwikkelaar kan daar berichten plaatsen.
                  </li>
                  <li>
                    6. De stemmen worden uitgebracht per e-mail op een wijze die de secretaris geschikt acht. De secretaris bepaalt voor elke stemming of de stemmers hun stem kunnen wijzigen.
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
        <ul>
          <li>
            A. Standaardprocedure voor een resolutie
            <ul>
              <li>
               3. Stemprocedure
                <ul>
                  <li>
                   4. Bij twijfel beslist de projectsecretaris over de procedure.
                  </li>
                </ul>
              </li>
            </ul>
	  </li>
        </ul>
      </blockquote>
    </div>

    <p>
      De volgende procedures zijn ingesteld met betrekking tot een voorstel tot algemene resolutie en steunbetuigingen ervoor.
    </p>

    <ol style="list-style-type: decimal">
      <li>
        De aangewezen elektronische mailinglijst is:
        <tt>debian-vote@lists.debian.org</tt>. Dit is de gezaghebbende bron van de volledige tekst van alle resoluties, alsmede voor de pleidooien ten gunste en ander materiaal. Voorstellen of steunbetuigingsmoties worden niet erkend als ze naar een andere mailinglijst worden gestuurd.
      </li>
      <li>
        Elk voorstel en elke e-mail met steunbetuigingen moet worden ondertekend met de cryptografische sleutel die in de Debian-sleutelbossen aanwezig is. De sleutelbossen maken deel uit van het gezaghebbende antwoord op wie wel of geen Debian-ontwikkelaar is.
      </li>
      <li>
        Bij elk voorstel moet duidelijk de begrenzing ervan aangegeven worden en het voorstel moet duidelijk worden afgebakend van begeleidende tekst in het e-mailbericht. Elke voorstander moet ook aangeven welke tekst hij ondersteunt, al was het maar om aan te geven dat hij de door de indiener gestelde begrenzing erkent.
      </li>
      <li>
        Wanneer de stemming wordt gehouden, moet de indiener of een voorstander van elk voorstel of amendement een definitieve versie van het voorstel of amendement in wml-formaat aanleveren om te worden opgenomen in de webpagina's van het Debian-project. Dit wml-fragment moet aantoonbaar exact de afgebakende en ondersteunde tekst bevatten.
      </li>
      <li>
        Wanneer tot de stemming wordt opgeroepen, moet de indiener of een voorstander van elk voorstel of amendement een samenvatting van één regel (60 tekens) van zijn voorstel of amendement geven. Deze samenvatting wordt door de secretaris in aanmerking genomen bij het opstellen van het stembiljet.
      </li>
    </ol>

    <p>
      Indien de punten 4 en 5 ontbreken, wordt de versie van de secretaris als definitief beschouwd. Er wordt sterk aangeraden dat de indieners en de voorstanders de zaak in kwestie vóór het einde van de minimale discussieperiode voorbereiden, aangezien de stemming niet zal worden uitgesteld vanwege deze ontbrekende elementen.
    </p>

    <h1>Aanbevolen werkwijzen en stijl</h1>

    <p>
      Het volgende is informatief in tegenstelling tot normatief. Deze suggesties zouden het gemakkelijker moeten maken om het voorstel te begrijpen, te volgen en erover te stemmen. Bij wijze van voorbeeld staat een <a
      href="sample_vote.template">sjabloon</a> ter beschikking dat men als uitgangspunt kan gebruiken.
    </p>

    <p>
      Het is een goede gewoonte om een beknopte beschrijving van het voorstel op te nemen in de onderwerpregel van de e-mail. Het wordt aanbevolen om de onderwerpregel op te maken als
      <q><em>Onderwerp: Voorstel - beschrijvende tekst</em></q>, dit helpt mensen bij het filteren van e-mails met betrekking tot het voorstel.
    </p>
    <p>
      Geef aan of uw voorstel een zelfstandig voorstel is, of een formeel amendement bij een bestaand voorstel. U kunt een onafhankelijk voorstel opstellen over hetzelfde onderwerp als een bestaand voorstel; het verschil is vooral een kwestie van timing. Een amendement krijgt mogelijk niet de volledige discussieperiode; en de stemming kan worden gehouden voordat u klaar bent met het verfijnen van uw amendement. Een onafhankelijk voorstel komt daarentegen misschien niet op hetzelfde stembiljet als het bestaande voorstel, afhankelijk van de timing, en er zouden twee onafhankelijke stemmingen kunnen plaatsvinden, één voor het bestaande voorstel en één voor uw nieuwe onafhankelijke voorstel. De uiteindelijke beslissing ligt bij u.
    </p>
    <p>
      Als uw voorstel een wijziging van de tekst van een bestaand document betreft, is het een goed idee om een echt patch-bestand (zoals gegenereerd door <code>diff -u</code>) aan te leveren, omdat het duidelijk laat zien welke wijzigingen worden voorgesteld, en het de voor het document verantwoordelijke persoon helpt als uw voorstel het haalt.
    </p>
    <p>
      Een suggestie voor indieners en ondersteuners is om de volledige tekst van het voorstel of het amendement in een apart (wellicht in het eigenlijke bericht opgenomen) ondertekend text/plain MIME-gedeelte te plaatsen, zodat er absoluut geen dubbelzinnigheid bestaat.
    </p>

    <h2><a name="amend">Amenderen</a></h2>

    <div style="border: solid; padding: 1ex">
      <blockquote>
        <ul>
          <li>
           A. Standaardprocedure voor een resolutie
            <ul>
              <li>
               1. Discussie en amendering
                <ul>
                  <li>
                   5. De indiener van een resolutie kan wijzigingen in de formulering van amendementen voorstellen; deze worden van kracht als de indiener van het amendement akkoord gaat en geen van de ondersteuners bezwaar heeft. In dat geval wordt over de gewijzigde amendementen gestemd in plaats van over de originele.
                  </li>
                  <li>
                   6. De indiener van een resolutie mag wijzigingen aanbrengen om kleine fouten (bijvoorbeeld tikfouten of inconsistenties) te corrigeren of wijzigingen die de strekking niet veranderen, mits niemand binnen 24 uur bezwaar maakt. In dat geval begint de minimumtermijn voor discussie niet opnieuw van vooraf aan te lopen.
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
      </blockquote>
    </div>
