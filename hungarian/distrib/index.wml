#use wml::debian::template title="A Debian beszerzése"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="fc74a202c0f1463d4bc08c88f3da3c2b4f1066f1" maintainer="Szabolcs Siebenhofer"

# translated by Viktor Nagy <chaotix@freemail.hu>
# updated by Viktor Nagy <chaotix@freemail.hu>

<p>A Debian <a href="../intro/free">szabadon</a> elérhető
az interneten. Letöltheted bármelyik <a href="ftplist">tükörszerverünkről</a>.
A <a href="../releases/stable/installmanual">Telepítési Leírás</a>
részletes telepítési instrukciókat tartalmaz.
És, a kiadási fejlegyzések megtalálhatóak <a href="../releases/stable/releasenotes">itt</a>.
</p>

<p>Ha telepíteni akarod a Debiant, a következőkből választhatsz:</p>

<div class="line">
   <div class="item col50">
  <h2><a href="netinst">Tölts le egy telepítő képfájlt</a></h2>
  <p>
  Az internetkapcsolatod sebességétől függően letöltheted az alábbiak egyikét:
  </p>
<ul>
      <li>Egy <a href="netinst"><strong>kisebb telepítő kép</strong></a>:
	    	    gyorsan letölthető és eltávolítható meghajtóra másolható. Ahhoz, hogy ezt használd, szükséged lesz egy számítógépre, aminek van internet kapcsolata.
	<ul class="quicklist downlist">
	  <li><a title="Telepítők letöltése 64-bites Intel and AMD PC-re"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64-bites
	      PC netinst lemezkép</a></li>
	  <li><a title="Telepítők letöltése normál 32-bites Intel and AMD PCre"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32-bit
	      PC netinst lemezkép</a></li>
	</ul>
    </li>
   <li>A nagyobb <a href="../CD/"><strong>teljes telepítő képek</strong></a>: több csomagot tartalmaznak, megkönnyítve ezzel a telepítést internet
   kapcsolat nélküli gépeken.
   <ul class="quicklist downlist">
	  <li><a title="64-bit Intel és AMD PC DVD torrentek letöltése"
	         href="<stable-images-url/>/amd64/bt-dvd/">64-bites PC torrentek (DVD)</a></li>
	  <li><a title="Normál 32-bites Intel and AMD PC DVD torrentek letöltése"
		 href="<stable-images-url/>/i386/bt-dvd/">32-bites PC torrentek (DVD)</a></li>
	  <li><a title="64-bit Intel és AMD PC CD torrentek letöltése"
	         href="<stable-images-url/>/amd64/bt-cd/">64-bites PC torrentek (CD)</a></li>
	  <li><a title="Normál 32-bites Intel and AMD PC CD torrentek letöltése"
		 href="<stable-images-url/>/i386/bt-cd/">32-bites DVD torrentek (CD)</a></li>
	</ul>
      </li>
    </ul>     
  </div>
  <div class="item col50 lastcol">
  <h2><a href="https://cloud.debian.org/images/cloud/">Debian felhő kép haználata</a></h2>
  <ul>
    <li>Hivatalos <a href="https://cloud.debian.org/images/cloud/"><strong>felhő kép</strong></a>:
          a felhő szolgáltatódnál közvetlenül használható, a Debian Cloud Csapat készítette.
      <ul class="quicklist downlist">
        <li><a title="OpenStack image for 64-bit Intel and AMD Qcow2" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2">64-bit AMD/Intel OpenStack (Qcow2)</a></li>
        <li><a title="OpenStack image for 64-bit ARM Qcow2" href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-arm64.qcow2">64-bit ARM OpenStack (Qcow2)</a></li>
      </ul>
    </li>
  </ul>
    <h2><a href="../CD/live/">Próbáld ki a Debian-t telepítés előtt</a></h2>
    <p>
      Ki tudod már próbálni a Debian-t úgy, hogy egy Live (működő) rendszert töltesz be CD-ről, DVD-ről vagy USB kulcsról, anélkül, hogy akár egy fájlt telepítenél a számítógépedre. Ha felkészültél, elindíthatod a beépített telepítőt (Debian 10 Buster-től kezdődően ez a
      felhasználó barát <a href="https://calamares.io">Calamares Installer</a>).
      Feltéve, hogy az eléhető képek megfelelnek a mérettel, nyevvel és csomagválasztékkal kapcsolatos elvárásaidnak, ez az eljárás megfelelő lehet neked.
      Olvasd el az <a href="../CD/live#choose_live">erről szóló informácókat</a>, hogy azok segítsenek a döntésben.
    </p>
    <ul class="quicklist downlist">
      <li><a title="Live torrentek letöltése 64-bites Intel and AMD PCre"
	     href="<live-images-url/>/amd64/bt-hybrid/">64 bites PC live torrentek</a></li>
    </ul>
  </div>
</div>
  
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">Vedd meg a CD-ket vagy DVD-ket a CD terjesztők valamelyikétől</a></h2>

   <p>
      Számos terjesztő 5$ + postaköltségnél olcsóbban árusítja a disztribúciót (ellenőrizd a weboldalukon, hogy külföldre is küldenek-e).
      <br />
      Némelyik <a href="../doc/books">Debianról szóló könyvhöz</a> mellékelik a CD-ket is.
   </p>

   <p>Itt vannak a mások által készített CD-k alapvető előnyei::</p>

 <ul>
   <li>A telepítés egyszerűbb.</li>
   <li>Internetkapcsolattal nem rendelkező gépeken is telepíthetsz.</li>
   <li>Nem neked kell letölteni a csomagokat.</li>
   <li>A CD segítségével egyszerűbben helyreállítható egy sérült Debian
   rendszer.</li>
</ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Vásárolj Debian-nal előre telepített számítógépet.</a></h2>
   <p>Ennek számos előnye van:</p>
   <ul>
    <li>Nem kell telepítened a Debiant.</li>
    <li>A telepítéset a hardvernek megfelelően végezték el.</li>
    <li>A szállító esetleg technikai támogatást is biztosít.</li>
   </ul>
  </div>
</div>
  
  
  
  
  
  
  
  
  
 
