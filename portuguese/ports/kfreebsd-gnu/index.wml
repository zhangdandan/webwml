#use wml::debian::template title="Debian GNU/kFreeBSD"
#use wml::debian::translation-check translation="1b39a9c0d2369a1c450d27f42462c2dc55c8aa37"

#use wml::debian::toc

<toc-display/>

<p>O Debian GNU/kFreeBSD é um porte que consiste de <a
href="https://www.gnu.org/">aplicações GNU em espaço de usuário(a)</a> que usam
<a href="https://www.gnu.org/software/libc/">a biblioteca GNU C</a> em cima do
kernel do <a href="https://www.freebsd.org/">FreeBSD</a>, acompanhado de um
conjunto regular de <a href="https://packages.debian.org/">pacotes do
Debian</a>.</p>

<div class="important">
O desenvolvimento do Debian GNU/kFreeBSD foi oficialmente encerrado em julho de
2023 devido à falta de interesse e de voluntários(as). Você pode encontrar o
<a href="https://lists.debian.org/debian-devel/2023/07/msg00176.html">anúncio
oficial aqui</a>.
</div>

<div class="important">
<p>O Debian GNU/kFreeBSD não é uma arquitetura oficialmente suportada.
Foi lançada com o Debian 6.0 (Squeeze) e o 7.0
(Wheezy) como uma <em>pré-estreia tecnológica</em> e o primeiro porte não
Linux. Desde o Debian 8 (Jessie) já não está mais incluída nos lançamentos
oficiais.</p>
</div>

<toc-add-entry name="resources">Recursos</toc-add-entry>

<p>Existem mais informações para o porte (incluindo um FAQ) na página wiki do
<a href="https://wiki.debian.org/Debian_GNU/kFreeBSD">Debian GNU/kFreeBSD</a>.
</p>

<h3>Listas de e-mail</h3>
<p><a href="https://lists.debian.org/debian-bsd">Lista de e-mail Debian GNU/k*BSD</a>.</p>

<h3>IRC</h3>
<p><a href="irc://irc.debian.org/#debian-kbsd">Canal #debian-kbsd no IRC</a> (em
irc.debian.org).</p>

<toc-add-entry name="Development">Desenvolvimento</toc-add-entry>

<p>Como utilizamos a biblioteca Glibc, os problemas de portabilidade são muito
simples e na maioria das vezes é apenas uma questão de copiar um caso de teste
para "k*bsd*-gnu" a partir de outro sistema baseado na Glibc (como o GNU ou o
GNU/Linux). Veja o documento sobre <a
href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">portabilidade</a>
para obter mais detalhes.</p>

<p>Veja também o arquivo <a
href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">TODO</a> para obter
mais detalhes sobre o que precisa ser feito.</p>

<toc-add-entry name="availablehw">Hardware disponível para desenvolvedores(as)
Debian</toc-add-entry>

<p>Para trabalhos de porte, os(as) desenvolvedores(as) Debian têm disponível
o lemon.debian.net (kfreebsd-amd64). Por favor, veja a <a
href="https://db.debian.org/machines.cgi">base de dados de máquinas</a> para
mais informações sobre essas máquinas. Em geral, você poderá utilizar dois
ambientes chroot: testing e unstable. Note que esses sistemas não são
administrados pelo DSA, portanto <b>não envie solicitações para o
debian-admin sobre isso</b>. Em vez disso, utilize o
<email "admin@lemon.debian.net">.</p>
